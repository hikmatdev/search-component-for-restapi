const express = require("express");
const app = express();
const _ = require("lodash");
const stores = require("./data/store");

app.get("/", function(req, res) {
  res.send("Hello World!");
});

//-------------------GET REQUEST WITH QUERIES------------------------
app.get("/api/stores", function(req, res) {
  var response = [];
  console.log(req.query);

  // this would usually adjust your database query
  if (typeof req.query.nsfw != "undefined") {
    Object.values(stores)(function(store) {
      if (store.nsfw.toString() == req.query.nsfw) {
        response.push(store);
      }
    });
  }

  if (typeof req.query.name != "undefined") {
    Object.values(stores).filter(function(store) {
      if (store.name.toString() == req.query.name) {
        response.push(store);
      }
    });
  }

  // this would usually adjust your database query
  if (typeof req.query.location != "undefined") {
    Object.values(stores).filter(function(store) {
      if (store.location === req.query.location) {
        response.push(store);
      }
    });
  }

  // de-duplication:
  response = _.uniqBy(response, "id");

  // in case no filtering has been applied, respond with all stores
  if (Object.keys(req.query).length === 0) {
    response = stores;
  }

  res.json(response);
});

app.listen(3000, function() {
  console.log("Example app listening on port 3000!");
});
